package com.emilylouden.com;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RetrieveValues {
	public String[] retrieve_values() {
		Connection connection = null;
		Statement statement = null;

		ConnectDB obj_ConnectDB = new ConnectDB();

		connection = obj_ConnectDB.get_connection();
		
		int id = -1;
		String lastName = null, firstName = null, address = null, password = null;

		try {
			statement = connection.createStatement();

			String query = "SELECT * FROM appUsers";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				id = rs.getInt("ID");
				lastName = rs.getString("LastName");
				firstName = rs.getString("FirstName");
				address = rs.getString("Address");
				password = rs.getString("Password");
				//System.out.println(id + "\t" + lastName + ",\t" + firstName + "\t" + address + "\t" + password);
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		// will return id = -1 and String null values if try block is unsuccessful
		String [] return_list = {Integer.toString(id), lastName, firstName, address, password};
		return return_list;
	}
}
